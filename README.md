Using STM32F411E Discovery card with the CUBE HAL Library and the development environment "System Workbench":

This example shows how to use the USB peripheral in transmit and recept mode (first go to: "USB_FILES repository" to understand how do you have to create a C project with USB library):
The user can press A or B.
If the key pressed is A every 1 sec, the message "Hello World!" is printed in the screen.

The most appropiate way to launch this example is: 
At the address: NAME_OF_PROJECT/inc and NAME_OF_PROJECT/src, replace the original files with those available in this repository. With these steps, the project can be compiled correctly.

at the file main.c:
  At the beginning of the main program the HAL_Init() function is called to reset all the peripherals, initialize the Flash interface and the systick. 
  Then the SystemClock_Config() function is used to configure the system clock (SYSCLK) to run at 84 MHz.
  
  RECEPTION
  
  The function void LSE_Receive_callback(USBD_HandleTypeDef* husbd, uint8_t* Buf, uint32_t *len) is used when the user send some data, so the library call the function "LSE_Receive_callback" (declared in usbd_cdc_if.c) to receive the message.
  Every time the user has pressed a key, the interruption is launched.
  
  The HAL_Systick_Callback function is programmed internally; every 1 msec the interruption is executed. 
  This can be used as an accountant. 
  The disadvantage is that the counter is activated when the main has been executed.
  
  TRANSMITION
  
  To send the data it is necessary to convert the numeric values obtained in a string of characters. 
  One way is converting the message to a local array using "sprintf"; that works the same as printf, with the difference that the impression of the message does not do it by the standard output; it stores the data in a chain of characters and passes it as the first argument.
  
  
  

